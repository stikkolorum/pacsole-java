package com.drstikko;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Console.Clear();
        Console.CursorVisible = false;
        ConsoleKey key = null; // different from C# code
        Level level1 = new Level();
        level1.Draw();
        Player player1 = new Player("Dave", 10, 3);
        while (key != ConsoleKey.Escape)
        {
            key = Console.ReadKey().Key;
            // the cases of the switch are different from C# code
            switch(key)
            {
                case RightArrow:
                    player1.Move(1,0);
                    break;
                case LeftArrow:
                    player1.Move(-1,0);
                    break;
                case UpArrow:
                    player1.Move(0, -1);
                    break;
                case DownArrow:
                    player1.Move(0, 1);
                    break;
                default:
                    break;
            }
        }
        //for closing the console window , not in C# code
        Console.Close();
    }
}
